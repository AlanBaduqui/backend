from django.db import models


class Pelicula(models.Model):
    titulo = models.CharField(max_length=150)
    estreno = models.IntegerField(default=2000)
    imagen = models.URLField(help_text="De imdb mismo")
    resumen = models.TextField(help_text="Descripción corta")

    class Meta:
        ordering = ['titulo']

    def __str__(self):
        return self.titulo


class Personaje(models.Model):
    nombre = models.CharField(max_length=150)
    nombre_Real = models.CharField(max_length=150)
    imagen = models.URLField(help_text="De imdb mismo")
    descripcion = models.TextField()
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre