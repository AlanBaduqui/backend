from django.contrib import admin
from .models import Pelicula, Personaje


admin.site.register(Pelicula)
admin.site.register(Personaje)

