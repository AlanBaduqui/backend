from django.shortcuts import render
from .models import Pelicula, Personaje
from .serializers import PeliculaSelializers, PersonajeSerializers
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

# Peliculas

class PeliculaListAPIView(ListAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSelializers

class PeliculaRetrieveAPIView(RetrieveAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSelializers

class PeliculaCreateAPIView(CreateAPIView):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSelializers

# Personajes

class PersonajeListAPIView(ListAPIView):
    queryset = Personaje.objects.all()
    serializer_class = PersonajeSerializers

class PersonajeRetrieveAPIView(RetrieveAPIView):
    queryset = Personaje.objects.all()
    serializer_class = PersonajeSerializers
