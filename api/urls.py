from django.urls import path

from api.views import PeliculaListAPIView, PeliculaRetrieveAPIView

urlpatterns = [
    path('', PeliculaListAPIView.as_view()),
    path('<pk>', PeliculaRetrieveAPIView.as_view()),
]
