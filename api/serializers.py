from .models import Pelicula, Personaje
from rest_framework.serializers import ModelSerializer

class PeliculaSelializers(ModelSerializer):
    class Meta:
        model = Pelicula
        fields= '__all__'

class PersonajeSerializers(ModelSerializer):
    class Meta:
        model = Personaje
        fields = '__all__'